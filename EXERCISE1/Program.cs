﻿using System;
using System.Collections.Generic;

namespace EXERCISE1
{
    class Program
    {
        static void segregateEvenOdd(List<int> arr)
        {
            /* Initialize left and right indexes */
            int left = 0, right = arr.Count - 1;
            while (left < right)
            {
                /* Increment left index while we see 0 at left */
                while (arr[left] % 2 == 0 && left < right)
                    left++;

                /* Decrement right index while we see 1 at right */
                while (arr[right] % 2 == 1 && left < right)
                    right--;

                if (left < right)
                {
                    /* Swap arr[left] and arr[right]*/
                    int temp = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;
                    left++;
                    right--;
                }
            }
        }
        static void MysegregateEvenOdd(List<int> arr)
        {
            int lastOddRecently = -1;
            int TranferOddValueTemp = 0;
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] % 2 != 0)
                {
                    if (lastOddRecently != i - 1)
                    {
                        TranferOddValueTemp = arr[i];
                        for (int j = i; j > lastOddRecently + 1; j--)
                        {
                            arr[j] = arr[j - 1];
                        }
                        arr[lastOddRecently + 1] = TranferOddValueTemp;
                    }
                    lastOddRecently += 1;
                }
            }
        }
        static void OutputArray(List<int> list)
        {
            string outputArrayGen = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (i != 0)
                {
                    outputArrayGen += "," + list[i];
                }
                else
                {
                    outputArrayGen += list[i];
                }
            }
            Console.WriteLine("Array Generation: [" + outputArrayGen + "]");
        }
        static void Main(string[] args)
        {
            List<int> listGeneration = new List<int>();

            Console.Write("Enter the number of elements n = ");
            int n = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Number is entering : " + n);
            for (int i = 0; i < n; i++)
            {
                Random rnd = new Random();
                int numberGen = rnd.Next(1, 1000);
                listGeneration.Add(numberGen);
            }
            OutputArray(listGeneration);

            // Sort list generation, odd to the left, even to the right
             MysegregateEvenOdd(listGeneration);
            //segregateEvenOdd(listGeneration);
            OutputArray(listGeneration);
        }
    }
}
